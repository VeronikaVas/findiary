package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Tests if both passwords match. Original password is obtained through
 * additional attribute "passwordComponentBinding"
 */
@FacesValidator("confirmPasswordValidator")
public class ConfirmPasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        UIInput passwdComponent = (UIInput) component.getAttributes().get("passwordComponentBinding");
        if (!passwdComponent.isValid()) {
            return;
        }

        String passwd = (String) passwdComponent.getValue();

        if (!value.equals(passwd)) {
            FacesMessage msg = new FacesMessage("Hesla se neshodují!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

}
