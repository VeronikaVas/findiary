package validators;

import api.UserManager;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 * Tests if the specified username already exists.
 *
 * @author Veronika Vasinova
 */
@RequestScoped
@Named("uniqueLoginValidator")
public class UniqueLoginValidator implements Validator {

    @EJB
    UserManager userManager;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (userManager.existUser((String) value)) {
            FacesMessage msg = new FacesMessage("Uživatel již existuje!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

}
