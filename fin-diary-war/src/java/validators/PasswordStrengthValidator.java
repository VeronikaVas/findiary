package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Tests password quality.
 */
@FacesValidator("passwordStrengthValidator")
public class PasswordStrengthValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String passwd = value.toString();
        int conditions = 0;
        for (int i = 0; i < passwd.length(); i++) {
            char c = passwd.charAt(i);
            if (Character.isUpperCase(c)) {
                conditions |= 0x01;
            }
            if (Character.isLowerCase(c)) {
                conditions |= 0x02;
            }
            if (Character.isAlphabetic(c)) {
                conditions |= 0x04;
            }
            if (Character.isDigit(c)) {
                conditions |= 0x08;
            }
        }
        if (conditions != 0x0f) {
            FacesMessage msg = new FacesMessage("Heslo je příliš slabé!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

}
