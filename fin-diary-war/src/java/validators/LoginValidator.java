/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validators;

import api.UserManager;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 * Tests if the specified user exists for username and password.
 *
 * @author Veronika Vasinova
 */
@RequestScoped
@Named("loginValidator")
public class LoginValidator implements Validator {

    @EJB
    UserManager userManager;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        UIInput passwdComponent = (UIInput) component.getAttributes().get("loginComponentBinding");
        // testuje, zda je heslo validni
        if (!passwdComponent.isValid()) {
            return;
        }

        // zsika hodnotu puvodniho hesla
        String loginN = (String) passwdComponent.getValue();
        if (userManager.checkLogin(loginN, userManager.getPasswdHash((String) value))) {
            FacesMessage msg = new FacesMessage("Špatné přihlašovací jméno nebo heslo!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

}
