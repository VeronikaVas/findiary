package util;

import static com.sun.xml.ws.spi.db.BindingContextFactory.LOGGER;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;

/**
 * Helper for getting dates.
 *
 * @author Veronika vasinova
 */
public class DatePicker {

    /**
     * Returns the date of the first day of the current month.
     *
     * @return minidate of actual month
     */
    public static Date getMinDate() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        LOGGER.log(Level.SEVERE, calendar.toString());
        return calendar.getTime();
    }

    /**
     * Returns the date of the last day of the current month.
     *
     * @return max date of actual month
     */
    public static Date getMaxDate() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

}
