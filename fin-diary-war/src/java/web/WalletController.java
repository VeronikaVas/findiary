package web;

import api.CurrencyManager;
import api.WalletManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import model.Currency;
import model.Wallet;

@RequestScoped
@Named("wallet")
public class WalletController implements Serializable {

    @EJB
    private WalletManager manager;

    @EJB
    CurrencyManager currencyManager;

    @Inject
    ActiveSession user;

    private boolean insertAction = false;
    private String name;
    private double balance;
    private Currency currency;

    private Wallet wallet;

    public WalletController() {
    }

    ;
    
    /**
     * Returns all wallets created by specific user.
     *
     * @return list of all user's wallets
     */
    public List<Wallet> getWallets() {
        return manager.getUserWallets(user.getUser());
    }

    /**
     * Deletes the selected wallet.
     *
     * @param wallet selected wallet
     * @return path to reload page
     */
    public String delete(Wallet wallet) {
        manager.deleteWallet(wallet);
        return "wallets";
    }

    /**
     * Saves values do db after submit form.
     *
     * @return path to previous page
     */
    public String saveForm() {
        if (insertAction) {
            manager.addWallet(user.getUser(), name, currency, balance);
        } else {
            manager.renameWallet(wallet, name);
        }
        return "wallets";
    }

    /**
     * Redirects the user to the page to edit wallet.
     *
     * @param wallet selected category
     * @return destination where the user would be redirected after click
     */
    public String edit(Wallet wallet) {
        this.wallet = wallet;
        this.balance = wallet.getBalance();
        this.currency = wallet.getCurrency();
        this.name = wallet.getName();
        return "wallet-edit";
    }

    /**
     * Redirects the user to the page to add wallet.
     *
     * @return destination where the user would be redirected after click
     */
    public String insert() {
        insertAction = true;
        return "wallet-edit";
    }

    /**
     * Converts strings to {@link Currency} and {@link Currency} to strings as
     * required.
     *
     * @return converter
     */
    public Converter getCurrencyConverter() {
        return currencyManager.getCurrencyConverter();
    }

    /**
     * Returns all currencies in db.
     *
     * @return list of all currencies
     */
    public List<Currency> getCurrencies() {
        return currencyManager.getCurrencies();
    }

    /**
     * Converts strings to {@link Wallet} and {@link Wallet} to strings as
     * required.
     *
     * @return converter
     */
    public Converter getWalletConverter() {
        return manager.getWalletConverter(user.getUser());
    }

    public boolean isInsertAction() {
        return insertAction;
    }

    public void setInsertAction(boolean insertAction) {
        this.insertAction = insertAction;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

}
