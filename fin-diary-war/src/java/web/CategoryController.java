package web;

import api.CategoryManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import model.Category;

/**
 *
 * @author Veronika Vasinova
 */
@RequestScoped
@Named("category")
public class CategoryController implements Serializable {

    @EJB
    CategoryManager catManager;

    @Inject
    ActiveSession user;

    private boolean insertAction = false;
    private String categoryName;
    private Category category;

    /**
     * Returns all categories created by specific user.
     *
     * @return list of all user's Categories
     */
    public List<Category> show() {
        return catManager.getCategories(user.getUser());
    }

    /**
     * Deletes the selected category.
     *
     * @param category select category
     * @return path to reload page
     */
    public String delete(Category category) {
        catManager.deleteCategory(category);
        return "categories";
    }

    /**
     * Saves values do db after submit form.
     *
     * @return path to previous page
     */
    public String saveForm() {
        if (insertAction) {
            catManager.addCategory(user.getUser(), categoryName);
        } else {
            catManager.updateName(category, categoryName);
        }
        return "categories";
    }

    /**
     * Converts strings to {@link Category} and {@link Category} to strings as
     * required.
     *
     * @return converter
     */
    public Converter getCategoryConverter() {
        return catManager.getCategoryConverter(user.getUser());
    }

    /**
     * Redirects the user to the page to edit category.
     *
     * @param c select category
     * @return destination where the user would be redirected after click
     */
    public String edit(Category c) {
        this.category = c;
        this.categoryName = c.getName();
        return "category-edit";
    }

    /**
     * Redirects the user to teh page to add category.
     *
     * @return destination where the user would be redirected after click
     */
    public String insert() {
        insertAction = true;
        return "category-edit";
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isInsertAction() {
        return insertAction;
    }

    public void setInsertAction(boolean insertAction) {
        this.insertAction = insertAction;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}
