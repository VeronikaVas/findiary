/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import api.RecordManager;
import api.WalletManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.Category;
import model.Wallet;
import util.DatePicker;

@SessionScoped
@Named("summary")
public class SummaryController implements Serializable {

    private Date dateFrom;
    private Date dateTo;
    private Wallet selectWallet;
    private Category selectCategory;
    
    private boolean generate = false;

    @EJB
    WalletManager walletManager;

    @EJB
    RecordManager recordManager;

    @Inject
    ActiveSession user;

    public SummaryController() {

    }

    private List<Wallet> getWallets() {
        return walletManager.getUserWallets(user.getUser());
    }

    public List<Category> getCategories() {
        if (selectCategory == null) {
            return walletManager.getWalletCategories(selectWallet);
        } else {
            List<Category> category = new ArrayList<>();
            category.add(selectCategory);
            return category;
        }
    }

    public Map<Category, List<Double>> getCategoriesSummary() {
        
        return recordManager.getCategoriesSummary(getCategories(), selectWallet, dateFrom, dateTo);
    }

    public double walletBalanceByCategory() {
        return walletManager.getWalletBalanceByCategory(getCategories(), selectWallet, dateFrom, dateTo);
    }
    
    
    public double getIncome(){
        return walletManager.getWalletIncome(getCategories(), selectWallet, dateFrom, dateTo);
    }
    
    public double getExpenses(){
        return walletManager.getWalletExpenses(getCategories(), selectWallet, dateFrom, dateTo);
    }
    
    public double getListSum(List<Double> list) {
        return list.stream().mapToDouble(Double::doubleValue).sum();
    }

    @PostConstruct
    public void init() {
        dateFrom = DatePicker.getMinDate();
        dateTo = DatePicker.getMaxDate();
        if(!getWallets().isEmpty())
            selectWallet = getWallets().get(0);
       
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Wallet getSelectWallet() {
        return selectWallet;
    }

    public void setSelectWallet(Wallet selectWallet) {
        this.selectWallet = selectWallet;
    }

    public Category getSelectCategory() {
        return selectCategory;
    }

    public void setSelectCategory(Category selectCategory) {
        this.selectCategory = selectCategory;
    }

    public boolean isGenerate() {
        return generate;
    }

    public void setGenerate(boolean generate) {
        this.generate = generate;
    }
    
    

}
