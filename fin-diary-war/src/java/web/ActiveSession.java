package web;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import model.User;

/**
 * Simple session containing actual user.
 *
 * @author Veronika Vasinova
 */
@SessionScoped
@Named("activeSession")
public class ActiveSession implements Serializable {

    private User user;

    /**
     *
     * @return active user of session
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user user for active session
     */
    public void setUser(User user) {
        this.user = user;
    }
}
