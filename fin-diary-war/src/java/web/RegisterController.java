package web;

import api.UserManager;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Controller for register form
 */
@Named("register")
@RequestScoped
public class RegisterController {

    @EJB
    private UserManager userManager;

    private String loginName;
    private String firstName;
    private String secondName;
    private String password;
    private String confirmPassword;
    private String mail;

    public RegisterController() {
        super();
    }

    /**
     * Creates new user and redirects user to login page.
     * @return path to login page
     */
    public String submit() {
        userManager.addUser(loginName, password, firstName, secondName, mail);
        return "login";
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
