/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import api.UserManager;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import model.User;

/**
 * Controller for sign in
 */
@RequestScoped
@Named("login")
public class LoginController {

    @EJB
    private UserManager userManager;
    @Inject
    private ActiveSession session;
    private String loginName;
    private String password;

    /**
     * Verify that a user with a login and password can sign in.
     *
     * @return destination where the user would be redirected after login
     */
    public String login() {
        User u = userManager.getUserLogin(loginName, userManager.getPasswdHash(password));
        if (u != null) {
            session.setUser(u);
        }
        return "summary";
    }

    /**
     * Redirects the user to the start page of the application.
     */
    public void redirect() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("summary.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Logs out user from active session.
     *
     * @return - destination where the user would be redirected after logout
     */
    public String logout() {
        session.setUser(null);
        return "index";
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
