package web;

import api.RecordManager;
import api.RecordTypeManager;
import api.WalletManager;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;

import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import model.Category;
import model.Record;
import model.RecordType;
import model.Wallet;
import util.DatePicker;

/**
 *
 * @author Veronika
 */
@SessionScoped
@Named("record")
public class RecordController implements Serializable {

    @EJB
    RecordManager manager;

    @EJB
    WalletManager walletManager;

    @EJB
    RecordTypeManager typeManager;

    @Inject
    ActiveSession user;

    private boolean insertAction = false;
    private Record record;

    private Wallet selectedWallet;
    private Category selectedCategory;
    private Date selectedDateFrom;
    private Date selectedDateTo;

    private Wallet wallet;
    private Category category;
    private double value;
    private Date date;
    private String note;
    private RecordType type;

    public RecordController() {
    }

    /**
     * Redirects the user to the page to add record.
     *
     * @return destination where the user would be redirected
     */
    public String insert() {
        clearForm();
        insertAction = true;
        return "record-edit";
    }

    /**
     * Returns all record filtered by wallet, category, date from and date to.
     *
     * @return list of selected records
     */
    public List<Record> getRecords() {
        if (selectedWallet != null || selectedCategory != null || selectedDateFrom != null || selectedDateTo != null) {
            return manager.getWalletRecords(selectedWallet,
                    selectedCategory,
                    selectedDateFrom,
                    selectedDateTo);
        } else {
            return manager.getUserRecords(user.getUser());

        }
    }

    /**
     * Deletes the selected record.
     *
     * @param r select record
     * @return path to reload page
     */
    public String delete(Record r) {
        manager.deleteRecord(r);
        clearForm();
        return "records";
    }

    /**
     * Saves values do db after submit insert/edit form.
     *
     * @return path to previous page
     */
    public String saveForm() {
        if (insertAction) {
            manager.addRecord(wallet, category, value, date, note, type);
        } else {
            record.setNote(note);
            record.setValue(value);
            record.setCategory(category);
            record.setDate(date);
            record.setOperation(type);
            manager.updateRecord(record);
        }
        clearForm();
        return "records";
    }

    private void clearForm() {
        insertAction = false;
        this.record = null;
        this.category = null;
        this.type = null;
        this.wallet = null;
        this.note = "";
        this.value = 0;
        this.date = new Date();
    }

    /**
     * Redirects the user to the page to edit selected record.
     *
     * @param r selected record category
     * @return destination where the user would be redirected after click
     */
    public String edit(Record r) {
        this.record = r;
        this.category = r.getCategory();
        this.value = r.getValue();
        this.date = r.getDate();
        this.type = r.getOperation();
        this.wallet = r.getWallet();
        this.note = r.getNote();
        return "record-edit";
    }

    /**
     * Sets the initial values for the filter - date from, date to and wallet.
     */
    @PostConstruct
    public void init() {
        selectedDateFrom = DatePicker.getMinDate();
        selectedDateTo = DatePicker.getMaxDate();
        List<Wallet> wallets = walletManager.getUserWallets(user.getUser());
        if (!wallets.isEmpty()) {
            selectedWallet = wallets.get(0);
        }

    }

    /**
     * Converts strings to {@link Record} and {@link Record} to strings as
     * required.
     *
     * @return converter
     */
    public Converter getRecordConverter() {
        return manager.getRecordsConverter();
    }

    /**
     * Converts strings to {@link RecordType} and {@link RecordType} to strings
     * as required.
     *
     * @return converter
     */
    public Converter getRecordTypeConverter() {
        return typeManager.getTypeConverter();
    }

    /**
     * Returns all record types.
     *
     * @return list of recor types
     */
    public List<RecordType> getRecordTypes() {
        return typeManager.getRecordTypes();
    }

    public boolean isInsertAction() {
        return insertAction;
    }

    public void setInsertAction(boolean insertAction) {
        this.insertAction = insertAction;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public RecordType getType() {
        return type;
    }

    public void setType(RecordType type) {
        this.type = type;
    }

    public Wallet getSelectedWallet() {
        return selectedWallet;
    }

    public void setSelectedWallet(Wallet selectedWallet) {
        this.selectedWallet = selectedWallet;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public Date getSelectedDateFrom() {
        return selectedDateFrom;
    }

    public void setSelectedDateFrom(Date selectedDateFrom) {
        this.selectedDateFrom = selectedDateFrom;
    }

    public Date getSelectedDateTo() {
        return selectedDateTo;
    }

    public void setSelectedDateTo(Date selectedDateTo) {
        this.selectedDateTo = selectedDateTo;
    }

}
