function deleteRow(text) {
    return confirm(text);
}


google.charts.load('current', {'packages': ['corechart']});
google.charts.setOnLoadCallback(drawChart);
// Draw the chart and set the chart values
function drawChart(value1, value2) {
    var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['Příjmy', value1],
        ['Výdaje', value2]
    ]);
    // Optional; add a title and set the width and height of the chart
    var options = {};
    var chart = new google.visualization.PieChart(document.getElementById('chart'));
    chart.draw(data, options);
}






