package api;

import java.util.List;
import javax.ejb.Local;
import javax.faces.convert.Converter;
import model.RecordType;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface RecordTypeManager {

    /**
     * Returns all record types(income, expenses).
     *
     * @return list of record types
     */
    public List<RecordType> getRecordTypes();

    /**
     * Returns {@link Converter} for all record types.
     *
     * @return {@link Converter}
     */
    public Converter getTypeConverter();

}
