package api;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.faces.convert.Converter;
import model.Category;
import model.Record;
import model.RecordType;
import model.User;
import model.Wallet;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface RecordManager {

    /**
     * Returns record filter by wallter, category and date from and date to.
     *
     * @param w selected wallet
     * @param c selected category
     * @param from date from
     * @param to date to
     * @return list of filtered records
     */
    public List<Record> getWalletRecords(Wallet w, Category c, Date from, Date to);

    /**
     * Returs records created by specific user u.
     *
     * @param u creator
     * @return list of filtered records
     */
    public List<Record> getUserRecords(User u);

    /**
     * Creates new record.
     *
     * @param w wallet
     * @param c category
     * @param value value
     * @param date date
     * @param name name of record
     * @param t record type
     */
    public void addRecord(Wallet w, Category c, double value, Date date, String name, RecordType t);

    /**
     * Updates the record r.
     *
     * @param r updated record
     */
    public void updateRecord(Record r);

    /**
     * Deletes the record r.
     *
     * @param r deleted record
     */
    public void deleteRecord(Record r);

    /**
     * Deletes all record in the category c.
     *
     * @param c selected category
     */
    public void deleteRecordInCategory(Category c);

    /**
     * Deletes all record in the wallet w.
     *
     * @param w selected wallet
     */
    public void deleteRecordsInWallet(Wallet w);

    /**
     * Converts strings to {@link Record} and {@link Record} to strings as
     * required.
     *
     * @return converter
     */
    public Converter getRecordsConverter();

    /**
     * Returs all records in db.
     *
     * @return list of all records
     */
    public List<Record> getRecords();

    /**
     * Returns sums of record types in category, filter by wallet and by date.
     *
     * @param categories
     * @param w wallet
     * @param from date from
     * @param to to
     * @return category + list of sums by record type
     */
    public Map<Category, List<Double>> getCategoriesSummary(List<Category> categories, Wallet w, Date from, Date to);

}
