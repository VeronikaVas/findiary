package api;

import java.util.List;
import javax.ejb.Local;
import javax.faces.convert.Converter;
import model.Category;
import model.User;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface CategoryManager {

    /**
     * Creates new category with the given name for the specific user.
     *
     * @param user the user of active session
     * @param name name of new category
     */
    public void addCategory(User user, String name);

    /**
     * Returns all categories created by specific user.
     *
     * @param user creator of categories
     * @return list of all Categories from db
     */
    public List<Category> getCategories(User user);

    /**
     * Deletes the specified category.
     *
     * @param category
     */
    public void deleteCategory(Category category);

    /**
     * Rename the specified category.
     *
     * @param category
     * @param name new category name
     */
    public void updateName(Category category, String name);

    /**
     * Returns {@link Converter} for categories.
     *
     * @param user for get only user's categories
     * @return {@link Converter}
     */
    public Converter getCategoryConverter(User user);

}
