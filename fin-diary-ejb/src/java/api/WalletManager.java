package api;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.faces.convert.Converter;
import model.Category;
import model.Currency;
import model.Record;
import model.User;
import model.Wallet;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface WalletManager {

    /**
     * Creates new wallet.
     *
     * @param user
     * @param name
     * @param currency
     * @param balance
     */
    public void addWallet(User user, String name, Currency currency, double balance);

    /**
     * Return all wallets created by user u
     *
     * @param user
     * @return list of user's wallets
     */
    public List<Wallet> getUserWallets(User user);

    /**
     * Deletes the wallet c and all all the records in it.
     *
     * @param wallet selected wallet
     */
    public void deleteWallet(Wallet wallet);

    /**
     * Rename the wallet with a new name.
     *
     * @param w
     * @param newName
     */
    public void renameWallet(Wallet w, String newName);

    /**
     * Converts strings to {@link Wallet} and {@link Wallet} to strings as
     * required. Returns wallet created by user u.
     *
     * @param u user
     * @return converter
     */
    public Converter getWalletConverter(User u);

    /**
     * Update balance in the wallet w with value and record type in record r.
     *
     * @param w wallet
     * @param r record
     */
    public void updateBalance(Wallet w, Record r);

    /**
     * Update balance in the wallet w with value and record type in record r.
     *
     * @param w wallet
     * @param r record
     */
    public void updateBalanceDeleting(Wallet w, Record r);

    /**
     * Returns categories that have a record in the wallet w.
     *
     * @param w wallet
     * @return list of selected categories
     */
    public List<Category> getWalletCategories(Wallet w);
    
    public double getWalletBalanceByCategory(List<Category> categories, Wallet w, Date from, Date to);
    public double getWalletIncome(List<Category> categories, Wallet w, Date from, Date to);
    public double getWalletExpenses(List<Category> categories, Wallet w, Date from, Date to);

}
