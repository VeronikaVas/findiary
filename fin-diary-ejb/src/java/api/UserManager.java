package api;

import javax.ejb.Local;
import model.User;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface UserManager {

    /**
     * Insert new user into database.
     *
     * @param loginName
     * @param password
     * @param firstName
     * @param secondName
     * @param mail
     */
    public void addUser(String loginName, String password, String firstName, String secondName, String mail);

    /**
     * Return user for given login name and password if exists in db.
     *
     * @param loginName
     * @param passwdHash
     * @return user for active session
     */
    public User getUserLogin(String loginName, String passwdHash);

    /**
     * Returns user by given login name;
     *
     * @param loginName
     * @return user
     */
    public User getUser(String loginName);

    /**
     * Encrypts entered password.
     *
     * @param passwd
     * @return encrypted password
     */
    public String getPasswdHash(String passwd);

    /**
     * Checks if a user with the login name already exists.
     *
     * @param loginName
     * @return true if user exists, otherwise false
     */
    public boolean existUser(String loginName);

    /**
     * Checks if the login name and password are correct
     *
     * @param loginName
     * @param passwdHash
     * @return true if login exists, otherwise false
     */
    public boolean checkLogin(String loginName, String passwdHash);

}
