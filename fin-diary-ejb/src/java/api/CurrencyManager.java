/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.util.List;
import javax.ejb.Local;
import javax.faces.convert.Converter;
import model.Currency;

/**
 *
 * @author Veronika Vasinova
 */
@Local
public interface CurrencyManager {

    /**
     * Returns all currencies.
     *
     * @return list of currencies
     */
    public List<Currency> getCurrencies();

    /**
     * Returns {@link Converter} for all currencies.
     *
     * @return {@link Converter}
     */
    public Converter getCurrencyConverter();

}
