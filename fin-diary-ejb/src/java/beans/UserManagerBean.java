package beans;

import api.UserManager;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.User;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class UserManagerBean implements UserManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @Override
    public void addUser(String loginName, String password, String firstName, String secondName, String mail) {
        User item = new User(loginName, getPasswdHash(password), firstName, secondName, mail);
        entityManager.merge(item);
    }

    @Override
    public User getUserLogin(String loginName, String passwdHash) {
        Query query = entityManager.createQuery("SELECT u from User u WHERE u.loginName = :loginName and u.passwordHash = :passwdHash");
        query.setParameter("loginName", loginName);
        query.setParameter("passwdHash", passwdHash);
        return (User) query.getSingleResult();
    }

    @Override
    public User getUser(String loginName) {
        Query query = entityManager.createQuery("SELECT u from User u where u.loginName = :loginName");
        query.setParameter("loginName", loginName);
        return (User) query.getSingleResult();
    }

    @Override
    public String getPasswdHash(String passwd) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(passwd.getBytes());
            return Base64.getEncoder().encodeToString(md.digest());
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean existUser(String loginName) {
        Query query = entityManager.createQuery("SELECT count(u) from User u where u.loginName = :loginName");
        query.setParameter("loginName", loginName);
        return (Long) query.getSingleResult() != 0;
    }

    @Override
    public boolean checkLogin(String loginName, String passwdHash) {
        Query query = entityManager.createQuery("SELECT count(u) from User u where u.loginName = :loginName and u.passwordHash=:pass");
        query.setParameter("loginName", loginName);
        query.setParameter("pass", passwdHash);
        return (Long) query.getSingleResult() == 0;
    }

}
