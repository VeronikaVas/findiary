package beans;

import api.RecordManager;
import api.RecordTypeManager;
import api.WalletManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Category;
import model.Currency;
import model.Record;
import model.RecordType;
import model.User;
import model.Wallet;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class WalletManagerBean implements WalletManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @EJB
    RecordManager recordManager;

    @EJB
    RecordTypeManager typeManager;

    @Override
    public void addWallet(User u, String name, Currency c, double balance) {
        entityManager.merge(new Wallet(name, u, c, balance));
    }

    @Override
    public List<Wallet> getUserWallets(User u) {
        Query query = entityManager.createQuery("SELECT item FROM Wallet item WHERE item.user=:user order by item.name");
        query.setParameter("user", u);
        return (List<Wallet>) query.getResultList();
    }

    @Override
    public void deleteWallet(Wallet c) {
        if (!entityManager.contains(c)) {
            c = entityManager.merge(c);
        }

        recordManager.deleteRecordsInWallet(c);
        entityManager.remove(c);
    }

    @Override
    public void renameWallet(Wallet w, String newName) {
        w.setName(newName);
        entityManager.merge(w);
    }

    @Override
    public Converter getWalletConverter(User u) {
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                for (Wallet item : getUserWallets(u)) {
                    if (item.getName().equals(value)) {
                        return item;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return (value != null) ? ((Wallet) value).getName() : null;
            }
        };
    }

    @Override
    public void updateBalance(Wallet w, Record r) {
        switch (r.getOperation().getOperation()) {
            case "+":
                w.setBalance(w.getBalance() + r.getValue());
                break;
            case "-":
                w.setBalance(w.getBalance() - r.getValue());
                break;
        }
        entityManager.merge(w);
    }

    @Override
    public void updateBalanceDeleting(Wallet w, Record r) {
        switch (r.getOperation().getOperation()) {
            case "+":
                w.setBalance(w.getBalance() - r.getValue());
                break;
            case "-":
                w.setBalance(w.getBalance() + r.getValue());
                break;
        }

        entityManager.merge(w);
    }

    @Override
    public List<Category> getWalletCategories(Wallet w) {
        Query q = entityManager.createQuery("SELECT distinct item.category FROM Record item WHERE item.wallet = :wallet");
        q.setParameter("wallet", w);
        return (List<Category>) q.getResultList();
    }

    /*@Override
    public List<Double> getWalletStatistics(Wallet w, Category c) {
        List<Double> o = new ArrayList<>();
        List<RecordType> types = typeManager.getRecordTypes();

        types.forEach((t) -> {
            Query q = entityManager.createQuery("SELECT sum(item.value) FROM Record item WHERE item.wallet = :wallet and item.category = :category and item.operation = :type");
            q.setParameter("wallet", w);
            q.setParameter("category", c);
            q.setParameter("type", t);
            Object r = q.getSingleResult();
            if (r == null) {
                o.add(0.0);
            } else {
                r = t.getOperation() + r;
                o.add(Double.valueOf(r.toString()));
            }
        });

        return o;
    }*/

    @Override
    public double getWalletBalanceByCategory(List<Category> categories, Wallet w, Date from, Date to) {
        Map<Category, List<Double>> values = recordManager.getCategoriesSummary(categories, w, from, to);
        double sum = 0;
        for (Map.Entry<Category, List<Double>> c : values.entrySet()) {
            sum += getListSum(c.getValue());
        }

        return sum;
    }
    
    @Override
    public double getWalletIncome(List<Category> categories, Wallet w, Date from, Date to){
        Map<Category, List<Double>> values = recordManager.getCategoriesSummary(categories, w, from, to);
        double sum = 0;
        for (Map.Entry<Category, List<Double>> c : values.entrySet()) {
            sum += c.getValue().get(1);
        }

        return Math.abs(sum);
    }

    @Override
    public double getWalletExpenses(List<Category> categories, Wallet w, Date from, Date to){
        Map<Category, List<Double>> values = recordManager.getCategoriesSummary(categories, w, from, to);
        double sum = 0;
        for (Map.Entry<Category, List<Double>> c : values.entrySet()) {
            sum += c.getValue().get(0);
        }

        return Math.abs(sum);
    }
    
    
    
    private double getListSum(List<Double> list) {
        return list.stream().mapToDouble(Double::doubleValue).sum();
    }

}
