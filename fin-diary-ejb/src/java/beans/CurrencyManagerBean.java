package beans;

import api.CurrencyManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Currency;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class CurrencyManagerBean implements CurrencyManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @Override
    public List<Currency> getCurrencies() {
        return (List<Currency>) entityManager.createQuery("SELECT item FROM Currency item").getResultList();
    }

    @Override
    public Converter getCurrencyConverter() {
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                for (Currency item : getCurrencies()) {
                    if (item.getCode().equals(value)) {
                        return item;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return ((Currency) value).getCode();
            }
        };
    }

}
