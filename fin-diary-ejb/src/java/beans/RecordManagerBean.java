/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import api.RecordManager;
import api.RecordTypeManager;
import api.WalletManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Category;
import model.Record;
import model.RecordType;
import model.User;
import model.Wallet;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class RecordManagerBean implements RecordManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @EJB
    WalletManager walletManager;

    @EJB
    RecordTypeManager typeManager;

    @Override
    public List<Record> getRecords() {
        return (List<Record>) entityManager.createQuery("SELECT item FROM Record item").getResultList();
    }

    @Override
    public List<Record> getWalletRecords(Wallet w, Category c, Date from, Date to) {
        String query = "SELECT item FROM Record item WHERE 1=1";

        if (w != null) {
            query += " and item.wallet = :wallet";
        }
        if (c != null) {
            query += " and item.category = :category";
        }

        if (from != null) {
            query += " and item.date >= :from";
        }

        if (to != null) {
            query += " and item.date <= :to";
        }

        Query q = entityManager.createQuery(query);

        if (w != null) {
            q.setParameter("wallet", w);
        }

        if (c != null) {
            q.setParameter("category", c);
        }

        if (from != null) {
            q.setParameter("from", from);
        }

        if (to != null) {
            q.setParameter("to", to);
        }

        return (List<Record>) q.getResultList();
    }

    @Override
    public void addRecord(Wallet w, Category c, double value, Date date, String name, RecordType t) {
        Record r = new Record(w, c, value, date, name, t);
        entityManager.merge(r);
        walletManager.updateBalance(w, r);
    }

    @Override
    public void updateRecord(Record r) {
        walletManager.updateBalanceDeleting(r.getWallet(), r);
        entityManager.merge(r);
        walletManager.updateBalance(r.getWallet(), r);

    }

    @Override
    public void deleteRecord(Record r) {
        if (!entityManager.contains(r)) {
            r = entityManager.merge(r);
        }
        walletManager.updateBalanceDeleting(r.getWallet(), r);
        entityManager.remove(r);
    }

    @Override
    public void deleteRecordInCategory(Category c) {
        Query q = entityManager.createQuery("DELETE FROM Record it" + " WHERE it.category = :cat");
        q.setParameter("cat", c);
        q.executeUpdate();

    }

    @Override
    public void deleteRecordsInWallet(Wallet w) {
        Query q = entityManager.createQuery("DELETE FROM Record it" + " WHERE it.wallet = :wallet");
        q.setParameter("wallet", w);
        q.executeUpdate();
    }

    @Override
    public Converter getRecordsConverter() {
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                for (Record item : getRecords()) {
                    if (item.getNote().equals(value)) {
                        return item;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return ((Record) value).getNote();
            }
        };
    }

    @Override
    public List<Record> getUserRecords(User u) {
        List<Wallet> wallets = walletManager.getUserWallets(u);
        if (wallets.isEmpty()) {
            return null;
        } else {
            Query q = entityManager.createQuery("SELECT item FROM Record item WHERE item.wallet in :wallets");
            q.setParameter("wallets", walletManager.getUserWallets(u));
            return (List<Record>) q.getResultList();
        }
    }

    private List<Double> getSumsByType(Wallet w, Category c, Date from, Date to) {
        List<Double> o = new ArrayList<>();
        List<RecordType> types = typeManager.getRecordTypes();

        types.forEach((t) -> {
            String query = "SELECT sum(item.value) FROM Record item WHERE item.wallet = :wallet and item.category = :category and item.operation = :type";
            if (from != null) {
                query += " and item.date >= :from";
            }

            if (to != null) {
                query += " and item.date <= :to";
            }
            Query q = entityManager.createQuery(query);
            q.setParameter("wallet", w);
            q.setParameter("category", c);
            q.setParameter("type", t);

            if (from != null) {
                q.setParameter("from", from);
            }

            if (to != null) {
                q.setParameter("to", to);
            }
            Object r = q.getSingleResult();
            if (r == null) {
                o.add(0.0);
            } else {
                r = t.getOperation() + r;
                o.add(Double.valueOf(r.toString()));
            }
        });

        return o;
    }
    
    
    @Override
    public Map<Category, List<Double>> getCategoriesSummary(List<Category> categories, Wallet w, Date from, Date to) {
        Map<Category, List<Double>> categoriesMap = new HashMap<>();
        categories.forEach((c) -> {
            categoriesMap.put(c, getSumsByType(w, c, from, to));
        });

        return categoriesMap;
    }
}
