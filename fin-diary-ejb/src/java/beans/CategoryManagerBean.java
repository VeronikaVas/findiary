/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import api.CategoryManager;
import api.RecordManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Category;
import model.User;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class CategoryManagerBean implements CategoryManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @EJB
    RecordManager recordManager;

    @Override
    public void addCategory(User u, String name) {
        entityManager.merge(new Category(name, u));
    }

    @Override
    public List<Category> getCategories(User user) {
        Query q = entityManager.createQuery("SELECT item FROM Category item WHERE item.user = :user");
        q.setParameter("user", user);
        return (List<Category>) q.getResultList();
    }

    @Override
    public void deleteCategory(Category c) {
        if (!entityManager.contains(c)) {
            c = entityManager.merge(c);
        }
        recordManager.deleteRecordInCategory(c);
        entityManager.remove(c);
    }

    @Override
    public void updateName(Category c, String name) {
        c.setName(name);
        entityManager.merge(c);

    }

    @Override
    public Converter getCategoryConverter(User user) {
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                for (Category item : getCategories(user)) {
                    if (item.getName().equals(value)) {
                        return item;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return (value != null) ? ((Category) value).getName() : null;
            }
        };
    }

}
