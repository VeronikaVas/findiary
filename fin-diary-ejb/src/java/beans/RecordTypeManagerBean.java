package beans;

import api.RecordTypeManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.RecordType;

/**
 *
 * @author Veronika Vasinova
 */
@Stateless
public class RecordTypeManagerBean implements RecordTypeManager {

    @PersistenceContext(unitName = "fin-diary-ejbPU")
    private EntityManager entityManager;

    @Override
    public List<RecordType> getRecordTypes() {
        return (List<RecordType>) entityManager.createQuery("SELECT item FROM RecordType item").getResultList();
    }

    @Override
    public Converter getTypeConverter() {
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                for (RecordType item : getRecordTypes()) {
                    if (item.getName().equals(value)) {
                        return item;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return ((RecordType) value).getName();
            }
        };
    }

}
